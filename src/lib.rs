//! # hafas-rest
//!
//! A library for interacting with the hafas API. The main API-endpoint is located at <https://v5.db.transport.rest/>
//! For documentation of the API, look at
//! <https://github.com/derhuerst/db-rest/blob/5/docs/api.md>.
//!
//! The main struct for interacting with the API is [Hafas], which uses [rrw](<https://crates.io/crates/rrw>) to initialize and use the API.

mod hafas;
mod structs;

pub use hafas::Hafas;
pub use structs::*;
