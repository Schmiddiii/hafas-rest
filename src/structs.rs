pub use queries::*;
pub use results::*;

mod queries {
    use chrono::{DateTime, Local};
    use serde::{Serialize, Serializer};

    /// Query [Stations][super::results::Station].
    #[derive(Serialize, Default, Clone)]
    pub struct StationsQuery {
        pub query: String,
        pub limit: Option<usize>,
        pub fuzzy: Option<bool>,
        pub completion: Option<bool>,
    }

    /// Query [Journey][super::results::Journey].
    #[derive(Serialize, Debug, Default, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct JourneysQuery {
        pub departure: Option<DateTime<Local>>,
        pub arrival: Option<DateTime<Local>>,
        pub from: Option<String>,
        pub to: Option<String>,
        pub earlier_than: Option<String>,
        pub later_than: Option<String>,
        pub results: Option<usize>,
        pub stopovers: Option<bool>,
        pub transfers: Option<usize>,
        pub transfer_time: Option<usize>,
        pub accessible: Option<String>,
        pub bike: Option<bool>,
        pub start_with_walking: Option<bool>,
        pub walking_speed: Option<String>,
        pub tickets: Option<bool>,
        pub polylines: Option<bool>,
        pub remarks: Option<bool>,
        pub schedule_days: Option<bool>,
        pub language: Option<String>,
        pub national_express: Option<bool>,
        pub national: Option<bool>,
        pub regional_exp: Option<bool>,
        pub regional: Option<bool>,
        pub suburban: Option<bool>,
        pub bus: Option<bool>,
        pub ferry: Option<bool>,
        pub subway: Option<bool>,
        pub tram: Option<bool>,
        pub taxi: Option<bool>,
        pub first_class: Option<bool>,
        pub loyalty_card: Option<LoyaltyCard>,
    }

    #[derive(Debug, Clone)]
    pub enum LoyaltyCard {
        BahnCard25Class1,
        BahnCard25Class2,
        BahnCard50Class1,
        BahnCard50Class2,
        Vorteilscard,
        HalbtaxaboRailplus,
        Halbtaxabo,
        VoordeelurenaboRailplus,
        Voordeelurenabo,
        SHCard,
        Generalabonnement,
    }

    impl Serialize for LoyaltyCard {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            let s = match self {
                LoyaltyCard::BahnCard25Class1 => "bahncard-1st-25",
                LoyaltyCard::BahnCard25Class2 => "bahncard-2nd-25",
                LoyaltyCard::BahnCard50Class1 => "bahncard-1st-50",
                LoyaltyCard::BahnCard50Class2 => "bahncard-2nd-50",
                LoyaltyCard::Vorteilscard => "vorteilscard",
                LoyaltyCard::HalbtaxaboRailplus => "halbtaxabo-railplus",
                LoyaltyCard::Halbtaxabo => "halbtaxabo",
                LoyaltyCard::VoordeelurenaboRailplus => "voordeelurenabo-railplus",
                LoyaltyCard::Voordeelurenabo => "voordeelurenabo",
                LoyaltyCard::SHCard => "shcard",
                LoyaltyCard::Generalabonnement => "generalabonnement",
            };
            serializer.serialize_str(s)
        }
    }

    impl LoyaltyCard {
        /// See https://gist.github.com/juliuste/202bb04f450a79f8fa12a2ec3abcd72d
        pub fn from_id(value: u8) -> Option<Self> {
            match value {
                1 => Some(LoyaltyCard::BahnCard25Class1),
                2 => Some(LoyaltyCard::BahnCard25Class2),
                3 => Some(LoyaltyCard::BahnCard50Class1),
                4 => Some(LoyaltyCard::BahnCard50Class2),
                9 => Some(LoyaltyCard::Vorteilscard),
                10 => Some(LoyaltyCard::HalbtaxaboRailplus),
                11 => Some(LoyaltyCard::Halbtaxabo),
                12 => Some(LoyaltyCard::VoordeelurenaboRailplus),
                13 => Some(LoyaltyCard::Voordeelurenabo),
                14 => Some(LoyaltyCard::SHCard),
                15 => Some(LoyaltyCard::Generalabonnement),
                _ => None,
            }
        }
    }

    /// Refresh a [Journey][super::results::Journey].
    #[derive(Serialize, Debug, Default, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct JourneysRefreshQuery {
        pub stopovers: Option<bool>,
        pub tickets: Option<bool>,
        pub polylines: Option<bool>,
        pub remarks: Option<bool>,
        pub language: Option<String>,
    }

    /// Query [Leg][super::results::Leg].
    #[derive(Serialize, Debug, Default, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct TripsQuery {
        pub line_name: String,
        pub stopovers: Option<bool>,
        pub remarks: Option<bool>,
        pub polylines: Option<bool>,
        pub language: Option<String>,
    }
}

mod results {
    use chrono::{DateTime, Local};
    use serde::{Deserialize, Serialize};

    /// A [Station] of the API.
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Station {
        #[serde(rename = "type")]
        pub type_: String,
        pub id: String,
        pub ril100: String,
        pub nr: Option<usize>,
        pub name: String,
        pub weight: f32,
        pub location: Location,
        pub operator: Option<Operator>,
        pub address: Address,
    }

    /// A [Location] of a [Station] or similar.
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    pub struct Location {
        #[serde(rename = "type")]
        pub type_: String,
        pub latitude: f32,
        pub longitude: f32,
    }

    /// The [Operator] of a [Station].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    pub struct Operator {
        #[serde(rename = "type")]
        pub type_: String,
        pub id: String,
        pub name: String,
    }

    /// The [Address] of a [Station].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    pub struct Address {
        pub city: String,
        pub zipcode: String,
        pub street: String,
    }

    /// The result of a [JourneysQuery][super::queries::JourneysQuery] holding [Journey]s and data to get earlier/later results.
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct JourneysResult {
        pub journeys: Vec<Journey>,
        pub earlier_ref: String,
        pub later_ref: String,
        pub realtime_data_from: usize,
    }

    /// A single [Journey] consisting of [Leg]s.
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Journey {
        pub legs: Vec<Leg>,
        pub refresh_token: String,
        pub cycle: Option<Cycle>,
        pub price: Option<Price>,
    }

    /// The cyclic time of a [Journey].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Cycle {
        pub min: Option<usize>,
        pub max: Option<usize>,
        pub nr: Option<usize>,
    }

    /// The price of a [Journey].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Price {
        pub amount: f32,
        pub currency: String,
        // hint?
    }

    /// A single mode of transport used by a [Journey].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Leg {
        pub origin: Stop,
        pub destination: Stop,
        pub departure: Option<DateTime<Local>>,
        pub planned_departure: Option<DateTime<Local>>,
        pub departure_delay: Option<usize>,
        pub arrival: Option<DateTime<Local>>,
        pub planned_arrival: Option<DateTime<Local>>,
        pub arrival_delay: Option<usize>,
        pub reachable: Option<bool>,
        // TripId and Id seem to be the same, but it seems that depending on the query, only one is
        // set.
        pub trip_id: Option<String>,
        pub id: Option<String>,
        pub line: Option<Line>,
        pub direction: Option<String>,
        pub current_location: Option<Location>,
        pub arrival_platform: Option<String>,
        pub planned_arrival_platform: Option<String>,
        pub departure_platform: Option<String>,
        pub planned_departure_platform: Option<String>,
        pub load_factor: Option<String>,
        pub alternatives: Option<Vec<Alternative>>,
        pub stopovers: Option<Vec<Stopover>>,
        pub remarks: Option<Vec<Remark>>,
    }

    impl Leg {
        pub fn id(&self) -> String {
            self.trip_id
                .as_ref()
                .unwrap_or(self.id.as_ref().unwrap_or(&"".to_string()))
                .clone()
        }
    }

    /// Remarks on a [Leg].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Remark {
        #[serde(rename = "type")]
        pub type_: String,
        pub text: String,
        pub code: Option<String>,
        pub summary: Option<String>,
    }

    /// Alternatives to a [Leg].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Alternative {
        pub trip_id: String,
        pub line: Option<Line>,
        pub direction: String,
        pub planned_when: Option<DateTime<Local>>,
        pub when: Option<DateTime<Local>>,
    }

    /// A single [Stop] of a [Leg].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Stop {
        #[serde(rename = "type")]
        pub type_: String,
        pub id: String,
        pub name: String,
        pub location: Location,
        pub products: Products,
    }

    /// A temporary stop of a [Leg].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Stopover {
        pub stop: Stop,
        pub departure: Option<DateTime<Local>>,
        pub planned_departure: Option<DateTime<Local>>,
        pub departure_delay: Option<usize>,
        pub arrival: Option<DateTime<Local>>,
        pub planned_arrival: Option<DateTime<Local>>,
        pub arrival_delay: Option<usize>,
        pub arrival_platform: Option<String>,
        pub planned_arrival_platform: Option<String>,
        pub departure_platform: Option<String>,
        pub planned_departure_platform: Option<String>,
    }

    /// Products available at a [Stop].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Products {
        pub national_express: bool,
        pub national: bool,
        pub regional_exp: bool,
        pub regional: bool,
        pub suburban: bool,
        pub bus: bool,
        pub ferry: bool,
        pub subway: bool,
        pub tram: bool,
        pub taxi: bool,
    }

    /// Information on the [Leg].
    #[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Line {
        #[serde(rename = "type")]
        pub type_: String,
        pub id: String,
        pub fahrt_nr: String,
        pub name: String,
        pub public: bool,
        pub admin_code: String,
        pub product_name: String,
        pub mode: String,
        pub product: String,
        pub operator: Option<Operator>,
    }
}
