use std::collections::HashMap;

use crate::structs::*;
use rrw::{Error, RestRequest, StandardRestError};
use rrw_macro::rest;

#[rest]
impl Hafas {
    /// Query [Station]s by a [StationsQuery].
    ///
    /// See <https://github.com/derhuerst/db-rest/blob/5/docs/api.md#get-stations>.
    pub async fn stations(
        &self,
        query: &StationsQuery,
    ) -> Result<HashMap<String, Station>, Error<StandardRestError>> {
        RestRequest::<&StationsQuery, ()>::get("/stations").query(query)
    }

    /// Query a station [Station] by id.
    ///
    /// See <https://github.com/derhuerst/db-rest/blob/5/docs/api.md#get-stationsid>.
    pub async fn stations_by_id<S: AsRef<str>>(
        &self,
        id: &S,
    ) -> Result<Station, Error<StandardRestError>> {
        RestRequest::<(), ()>::get(format!("/stations/{}", id.as_ref()))
    }

    /// Query [Journey]s by a [JourneysQuery].
    ///
    /// See <https://github.com/derhuerst/db-rest/blob/5/docs/api.md#get-journeys>.
    pub async fn journey(
        &self,
        query: &JourneysQuery,
    ) -> Result<JourneysResult, Error<StandardRestError>> {
        RestRequest::<&JourneysQuery, ()>::get("/journeys").query(query)
    }

    /// Refresh a [Journey] by a refresh id and [JourneysRefreshQuery].
    ///
    /// See <https://github.com/derhuerst/db-rest/blob/5/docs/api.md#get-journeysref>.
    pub async fn journey_refresh<S: AsRef<str>>(
        &self,
        refresh_id: S,
        query: &JourneysRefreshQuery,
    ) -> Result<Journey, Error<StandardRestError>> {
        RestRequest::<&JourneysRefreshQuery, ()>::get(format!("/journeys/{}", refresh_id.as_ref()))
            .query(query)
    }

    /// Query a [Leg] by a trip id and [TripsQuery].
    ///
    /// See <https://github.com/derhuerst/db-rest/blob/5/docs/api.md#get-tripsid>.
    pub async fn trips<S: AsRef<str>>(
        &self,
        trip_id: S,
        query: &TripsQuery,
    ) -> Result<Leg, Error<StandardRestError>> {
        RestRequest::<&TripsQuery, ()>::get(format!("/trips/{}", trip_id.as_ref())).query(query)
    }
}

#[cfg(test)]
mod test {
    use chrono::Local;
    use rrw::RestConfig;

    use super::*;

    #[tokio::test]
    async fn stations() -> Result<(), Box<dyn std::error::Error>> {
        let hafas = Hafas::new(RestConfig::new("https://v5.db.transport.rest"));
        let query = StationsQuery {
            query: "dammt".to_string(),
            limit: None,
            fuzzy: None,
            completion: None,
        };

        let results = hafas.stations(&query).await?;

        assert!(results.contains_key("8002548"));
        assert_eq!(results.get("8002548").unwrap().name, "Hamburg Dammtor");

        Ok(())
    }

    #[tokio::test]
    async fn station() -> Result<(), Box<dyn std::error::Error>> {
        let hafas = Hafas::new(RestConfig::new("https://v5.db.transport.rest"));
        let results = hafas.stations_by_id(&"8010159").await?;

        assert_eq!(results.name, "Halle (Saale) Hbf");

        Ok(())
    }

    #[tokio::test]
    async fn journey() -> Result<(), Box<dyn std::error::Error>> {
        let hafas = Hafas::new(RestConfig::new("https://v5.db.transport.rest"));
        let journey = JourneysQuery {
            departure: Some(Local::now()),
            from: Some("8010159".to_string()),

            to: Some("8098205".to_string()),
            ..Default::default()
        };
        let result = hafas.journey(&journey).await?;

        // Cannot really assert many things, as time table is dynamic.
        // Maybe mock?
        assert!(!result.journeys.is_empty());

        Ok(())
    }

    #[tokio::test]
    async fn journey_refresh() -> Result<(), Box<dyn std::error::Error>> {
        let hafas = Hafas::new(RestConfig::new("https://v5.db.transport.rest"));
        let journey = JourneysQuery {
            departure: Some(Local::now()),
            from: Some("8010159".to_string()),
            to: Some("8098205".to_string()),
            ..Default::default()
        };
        let result = hafas.journey(&journey).await?;
        let refresh = hafas
            .journey_refresh(
                &result.journeys[0].refresh_token,
                &JourneysRefreshQuery::default(),
            )
            .await?;

        assert_eq!(result.journeys[0].legs[0].trip_id, refresh.legs[0].trip_id);

        Ok(())
    }

    #[tokio::test]
    async fn trips() -> Result<(), Box<dyn std::error::Error>> {
        env_logger::init();
        let hafas = Hafas::new(RestConfig::new("https://v5.db.transport.rest"));
        let journey = JourneysQuery {
            departure: Some(Local::now()),
            from: Some("8010159".to_string()),
            to: Some("8098205".to_string()),
            ..Default::default()
        };
        let result = hafas.journey(&journey).await?;
        let leg = &result.journeys[0].legs[0];

        let trip = hafas
            .trips(
                &leg.id(),
                &TripsQuery {
                    line_name: leg.line.as_ref().expect("Leg not set up").name.clone(),
                    ..Default::default()
                },
            )
            .await?;

        assert_eq!(leg.id(), trip.id());

        Ok(())
    }
}
